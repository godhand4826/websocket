package main

import (
	"errors"
	"io"
	"net"
	"sync"
	"sync/atomic"
	"syscall"

	"go.uber.org/zap"
	"golang.org/x/net/websocket"
)

type ChatServer struct {
	connCount atomic.Int64
	connSet   sync.Map
	logger    *zap.Logger
}

func NewChatServer(logger *zap.Logger) *ChatServer {
	return &ChatServer{
		logger: logger,
	}
}

func (s *ChatServer) Handle(ws *websocket.Conn) {
	s.connSet.Store(ws, struct{}{})
	defer s.connSet.Delete(ws)

	s.logger.Info("websocket handle start",
		zap.String("origin", ws.RemoteAddr().String()),
		zap.Int64("connections", s.connCount.Add(1)))
	defer func() {
		s.logger.Info("websocket handle end",
			zap.String("origin", ws.RemoteAddr().String()),
			zap.Int64("connections", s.connCount.Add(-1)))
	}()

	s.Read(ws)
}

func (s *ChatServer) Read(ws *websocket.Conn) {
	for {
		var msg string

		if err := websocket.Message.Receive(ws, &msg); err != nil {
			if err != io.EOF && !errors.Is(err, syscall.ECONNRESET) && !errors.Is(err, net.ErrClosed) {
				s.logger.Error("Error reading message", zap.Error(err))
			}
			ws.Close()
			return
		}

		s.logger.Debug("Received message", zap.String("msg", msg))

		s.Broadcast(msg)
	}
}

func (s *ChatServer) Broadcast(msg string) {
	s.connSet.Range(func(key, _ any) bool {
		ws := key.(*websocket.Conn)

		if err := websocket.Message.Send(ws, msg); err != nil {
			if err != io.EOF && !errors.Is(err, syscall.EPIPE) && !errors.Is(err, syscall.ECONNRESET) && !errors.Is(err, net.ErrClosed) {
				s.logger.Error("Error sending message", zap.Error(err))
			}
			ws.Close()
			return true // continue
		}

		return true
	})
}
