package main

import (
	"io"
	"sync/atomic"

	"go.uber.org/zap"
	"golang.org/x/net/websocket"
)

type EchoServer struct {
	connCount atomic.Int64
	logger    *zap.Logger
}

func NewEchoServer(logger *zap.Logger) *EchoServer {
	return &EchoServer{
		logger: logger,
	}
}

func (s *EchoServer) Handle(ws *websocket.Conn) {
	s.logger.Info("websocket handle start",
		zap.String("origin", ws.RemoteAddr().String()),
		zap.Int64("connections", s.connCount.Add(1)))
	defer func() {
		s.logger.Info("websocket handle end",
			zap.String("origin", ws.RemoteAddr().String()),
			zap.Int64("connections", s.connCount.Add(-1)))
	}()
	io.Copy(ws, ws)
}
