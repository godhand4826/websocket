package main

import (
	"net/http"

	"go.uber.org/zap"
	"golang.org/x/net/websocket"
)

func main() {
	config := zap.NewDevelopmentConfig()
	config.Level = zap.NewAtomicLevelAt(zap.InfoLevel)
	logger, _ := config.Build()
	defer logger.Sync()

	echo := NewEchoServer(logger.With(zap.String("server", "echo")))
	chat := NewChatServer(logger.With(zap.String("server", "chat")))

	http.Handle("/echo", websocket.Handler(echo.Handle))
	http.Handle("/chat", websocket.Handler(chat.Handle))

	logger.Info("Starting server", zap.String("address", ":8282"))
	if err := http.ListenAndServe(":8282", nil); err != nil && err != http.ErrServerClosed {
		logger.Error("Error starting server", zap.Error(err))
	}
}
