import ws from 'k6/ws';
import { check, sleep } from 'k6';

export const options = {
	vus: 1000,
	duration: '30s',
};

export default function () {
	const url = 'ws://localhost:8282/echo';
	const params = { headers: { Origin: 'http://localhost:8282' } }

	sleep(Math.random() * 5);

	const response = ws.connect(url, params, function (socket) {
		let received = 0
		let sent = 0

		socket.on('open', function open() {
			console.log('connected');

			// send 20 messages
			for (let i = 0; i < 20; i++) {
				sent += 1
				socket.send(Date.now())
			}

			// closing the socket after 1 seconds
			socket.setTimeout(() => socket.close(), 1000);
		});
		socket.on('ping', () => console.log('ping!'));
		socket.on('pong', () => console.log('pong!'));
		socket.on('close', () => console.log(`disconnected. sent=${sent} received=${received}`));
		socket.on('message', () => received += 1)
		socket.on('error', (e) => {
			if (e.error() != 'websocket: close sent') {
				console.log('An unexpected error occurred: ', e.error());
			}
		});
	});

	check(response, { 'status is 101': (r) => r && r.status === 101 });
}

