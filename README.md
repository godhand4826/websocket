# websocket

### Dependency
- go 1.20
- k6 v0.47.0
- node v16.20.2

### start server
```
go build . && ./websocket
```

### bench with k6
```
npm ci && npm run bench
```