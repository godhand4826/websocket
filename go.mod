module websocket

go 1.20

require (
	go.uber.org/zap v1.26.0
	golang.org/x/net v0.19.0
)

require go.uber.org/multierr v1.10.0 // indirect
